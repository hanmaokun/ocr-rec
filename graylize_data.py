# -*- coding: utf-8 -*-

import os
import fnmatch
import cv2
import numpy as np


dst_dir = '/home/nlp/devel/ocr/cnn-lstm-ctc/dataset/english_sentence/split_tiny_images'

for root, dir_names, file_names in os.walk(dst_dir):
    for file_name in fnmatch.filter(file_names, '*.png'):
        img_path = os.path.join(root, file_name)
        img = cv2.imread(img_path)
        gray_img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        img_path = img_path[:-4] + '.jpg'
        cv2.imwrite(img_path, gray_img)
