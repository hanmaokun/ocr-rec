import os
import fnmatch
import xml.etree.cElementTree as ET
import cv2
import random

dst_dir = '/home/nlp/devel/ocr/cnn-lstm-ctc/dataset/english_sentence/split_tiny_images'

minW = 2000
minH = 2000
maxW = 0
maxH = 0

rec0 = 'none'
rec1 = 'none'
rec2 = 'none'
rec3 = 'none'

for root, dir_names, file_names in os.walk(dst_dir):
    for img_name in fnmatch.filter(file_names, '*.jpg'):
        img_path = os.path.join(root, img_name)
        print img_path
        img = cv2.imread(img_path)
        width = img.shape[0]
        height = img.shape[1]

        if width < minW:
            minW = width
            rec0 = img_path
        elif width > maxW:
            maxW = width
            rec1 = img_path

        if height < minH:
            minH = height
            rec2 = img_path

        elif height > maxH:
            maxH = height
            rec3 = img_path

print("height from: " + str(minW) + " to " + str(maxW) + ", width from " + str(minH) + " to " + str(maxH))
print(rec0)
print(rec1)
print(rec2)
print(rec3)