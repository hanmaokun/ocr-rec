if ! [ -d snapshot ]; then
   echo "creating snapshot folder"
   mkdir snapshot
fi
#python -u train/train.py ./dataset/english_sentence 2>&1 |tee log.txt
#THEANO_FLAGS=mode=FAST_RUN,device=gpu,floatX=float32 python -u train/train.py ./dataset/english_sentence 2>&1 |tee log.txt
#THEANO_FLAGS=device=cuda0 python -u train/train.py ./dataset/english_sentence 2>&1 |tee log.txt
THEANO_FLAGS=mode=FAST_RUN,device=gpu,floatX=float32,optimizer=None python -u train/train.py ./dataset/english_sentence 2>&1 |tee log.txt

