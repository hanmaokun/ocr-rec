# -*- coding: utf-8 -*-

import os
import fnmatch
import xml.etree.cElementTree as ET
import cv2
import random
import numpy as np
import re

zhPattern = re.compile(u'[\u4e00-\u9fa5]+')
abnormalPattern = re.compile(r'[!-:]')

src_dir = '/home/nlp/bigsur/devel/anno_data_/online'
dst_dir = '/home/nlp/devel/ocr/cnn-lstm-ctc/dataset/english_sentence/split_tiny_images_date'

n = 1
zh_ctr = 0
abnormal_ctr = 0
not_qualified_file_ctr = 0
img_size_nofitting_ctr = 0
large_width_ctr = 0

if not os.path.isdir(dst_dir):
    os.makedirs(dst_dir)

def file_extension(path):
  return os.path.splitext(path)[1]


def rotate_bound(image, angle):
    # grab the dimensions of the image and then determine the
    # center
    (h, w) = image.shape[:2]
    (cX, cY) = (w // 2, h // 2)

    # grab the rotation matrix (applying the negative of the
    # angle to rotate clockwise), then grab the sine and cosine
    # (i.e., the rotation components of the matrix)
    M = cv2.getRotationMatrix2D((cX, cY), -angle, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])

    # compute the new bounding dimensions of the image
    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))

    # adjust the rotation matrix to take into account translation
    M[0, 2] += (nW / 2) - cX
    M[1, 2] += (nH / 2) - cY

    # perform the actual rotation and return the image
    return cv2.warpAffine(image, M, (nW, nH))

# Generate all images and info file.
f = open(os.path.join(dst_dir, 'RiQi.txt'), 'w')
f.write(str(32)+' '+str(60)+'\n')

#rot_dic = {None:0, 'None': 0, '2':0, '3':0, '4':0, '5':0, 'b': 90, 'c': 180, 'd':270}
rot_dic = {'b': 90, 'c': 180, 'd':270}

for root, dir_names, file_names in os.walk(src_dir):
    for file_name in fnmatch.filter(file_names, '*.xml'):
        file_path = os.path.join(root, file_name)
        tree = ET.ElementTree(file=file_path)
        tree_root = tree.getroot()
        objs = tree_root.findall('object')
        paths = tree_root.findall('path')
        for path in paths:
            suffix = file_extension(path.text)

        not_qualified_file = True
        rot_angel = 0
        for obj in objs:
            img_name = file_name[:-4] + str(suffix)
            img_path = os.path.join(root, img_name)
            if obj.find('name').text in ['ShouJu_MenZhen','ShouJu_ZhuYuan']:
                not_qualified_file = False
                rotation_value = obj.find('value').text
                if rotation_value in rot_dic:
                    rot_angel = rot_dic[rotation_value]
                    #print(str(rot_angel) + ' ' + img_path)
                else:
                    rot_angel = 0

        if not_qualified_file:
            not_qualified_file_ctr += 1
            continue

        rotation_annotation_wrong = False
        image_size_not_fitting = False
        for obj in objs:
            if obj.find('name').text == 'RiQi':
                bndbox = obj.find('bndbox')
                xmin = int(bndbox.find('xmin').text)
                xmax = int(bndbox.find('xmax').text)
                ymin = int(bndbox.find('ymin').text)
                ymax = int(bndbox.find('ymax').text)

                #print img_path
                try:
                    img = cv2.imread(img_path)
                    content = img[ymin:ymax, xmin:xmax, :]
                except:
                    print('Exception: ' + img_path + ' ' + str(ymin) + ' ' + str(ymax) + ' ' + str(xmin) + ' ' + str(xmax))
                    continue

                height = content.shape[0]
                width = content.shape[1]
                if height > width:
                    rotation_annotation_wrong = True
                    #print('WRONG WITH ' + img_path + ' ' + str(n) + '.jpg')

                if height < 20 or height > 228 or width < 30:
                    image_size_not_fitting = True

                if image_size_not_fitting:
                    img_size_nofitting_ctr += 1
                    continue
                '''
                cv2.namedWindow('RiQi')
                cv2.imshow('RiQi', date)
                cv2.waitKey(0)
                cv2.destroyAllWindows()
                '''
                num_value = obj.find('value').text
				if not num_value or num_value=='None' or num_value=='0':
				    continue

                if zhPattern.search(num_value):
                    zh_ctr += 1
                    continue
                '''
                if not re.match('^[0-9a-z]+.', num_value):
                    print(num_value)
                    continue
                '''
                if not abnormalPattern.search(num_value):
                    abnormal_ctr += 1
                    continue

                resize_ratio = height/28.
                resized_width = width/resize_ratio
                if resized_width > 488:
                    large_width_ctr += 1
                    continue

                content = cv2.resize(content, (int(resized_width), 28), interpolation=cv2.INTER_CUBIC)

                if not rotation_annotation_wrong:
                    new_name = str(n)+'.jpg'
                    f.write(new_name+' ')

                    #print num_value
                    lists = map(ord, num_value)
                    for ch in lists:
                        if ch < 32 or ch > 59:
                            print('abnormal case ' + num_value + ' ' + img_path)
                        f.write(str(ch) + ' ')
                    f.write('\n')

                    dst_file = os.path.join(dst_dir, str(n)+'.jpg')
                    cv2.imwrite(dst_file, content)
                    n += 1
f.close()
print('zh ' + str(zh_ctr))
print('abnormal ' + str(abnormal_ctr))
print('not qualified file ' + str(not_qualified_file_ctr))
print('img_size_nofitting ' + str(img_size_nofitting_ctr))
print('large_width ' + str(large_width_ctr))
