import os
import fnmatch
import xml.etree.cElementTree as ET
import cv2
import random

dst_dir = '/home/nlp/devel/ocr/cnn-lstm-ctc/dataset/english_sentence/split_tiny_images'

if not os.path.isdir(dst_dir):
    os.makedirs(dst_dir)

# Generate all images and info file.
f = open(os.path.join(dst_dir, 'RiQi.txt'), 'r')

# Split to train val test file.
f_train = open('train_img_list.txt', 'w')
f_val = open('val_img_list.txt', 'w')
f_test = open('test_img_list.txt', 'w')

random.seed(100)
line_idx = 0
for line in f.readlines():
    line_idx += 1
    if line_idx == 1:
        header = line
        f_train.write(header)
        f_val.write(header)
        f_test.write(header)
    else:
        RDM = random.randint(1, 100)
        if RDM < 5:
            f_test.write(line)
        elif RDM < 12:
            f_val.write(line)
        else:
            f_train.write(line)

f.close()


