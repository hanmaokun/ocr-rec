# -*- coding: utf-8 -*-
import os
import fnmatch
import xml.etree.cElementTree as ET
import random
import numpy as np
import re
from subprocess import call

src_dir = '/home/nlp/devel/ocr/cnn-lstm-ctc/dataset/english_sentence/split_tiny_images_sn/'
dst_dir = '/home/nlp/devel/ocr/cnn-lstm-ctc/dataset/english_sentence/split_tiny_images_sn_bin'
#src_dir = '/Users/hanmaokun/Downloads/bin_test_src/'
#dst_dir = '/Users/hanmaokun/Downloads/bin_test_des'

total_imgs_num = 2243

if not os.path.isdir(dst_dir):
    os.makedirs(dst_dir)

# concat src image files name
src_files_list_str = ' '
for i in xrange(total_imgs_num):
    file_path = src_dir + str(i+1) + '.jpg'
    src_files_list_str += ' ' + file_path

#print(src_files_list_str)

# compose cmdline
execution_cmdline = 'ocropus-nlbin -n ' + src_files_list_str + ' -o ' + dst_dir
print(execution_cmdline)

# run
#os.system(execution_cmdline)
#call(['ocropus-nlbin -n ', src_files_list_str, ' -o ', dst_dir])
#os.system(execution_cmdline)
os.popen(execution_cmdline)
