import os, sys

src_fname = 'RiQi_ori.txt'
dst_fname = 'RiQi.txt'

f_out = open(dst_fname, 'w')

with open(src_fname, 'r') as f:
	lines = f.readlines()
	chars_from, chars_to = lines[0].split(' ')
	f_out.write(chars_from + ' ' + chars_to)
	for r in lines[1:]:
		fields = r.strip().split(' ')
		f_out.write(fields[0])
		for ch in fields[1:]:
			new_ch = str(int(ch) - int(chars_from))
			f_out.write(' ' + new_ch)
		f_out.write('\n')

		#print fields
